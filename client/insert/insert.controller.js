(function () {
    'use strict';
    
    angular
        .module("GroceryApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http", "$filter","InsertService"];

    function InsertCtrl($http, $filter,InsertService) {
        console.log("-- insert.controller.js");
        
        // Exposed variables
        var vm = this;
        vm.brand = {};
        vm.product = {};
        vm.status = {
            message: "",
            success: ""
        };

        // Exposed functions
        vm.register = register;

        // Initial calls
        initGroceryList();

        // Function definition
        function initGroceryList(){
            console.log("-- insert.controller.js > initGroceryList()");
            InsertService.intiItem()
                .then(function (response) {
                    vm.brand = response;
                })
                .catch(function (error) {
                    
                })
        }

        function register() {
            // We need to add selected item's start and expiry date
            vm.product.from_date = $filter('date')(new Date(), 'yyyy-MM-dd');
            vm.prodcut.to_date = new Date('9999-01-01');
            console.log("vm.brand" + JSON.stringify(vm.brand));

            InsertService.register(vm.brand)
                .then(function (response) {
                    vm.brand = response;
                    vm.status.message = "The brand is added to the database.";
                    vm.status.success = "ok";
                })
                .catch(function (error) {
                    vm.status.message = "Failed to add the brand to the database.";
                    vm.status.success = "error";
                }) ;
            
            
        }
    } // END InsertCtrl

})();






