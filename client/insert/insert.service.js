(function () {
    angular
        .module("GroceryApp")
        .service("InsertService",InsertService);
    InsertService.$inject=["$http","$q"];
    function  InsertService($http,$q) {
        var vm=this;


        vm.register=function (brandInfo) {
          var defer = $q.defer();
            $http.post("/api/brands & product",brandInfo)
                .then(function (response) {
                    
                   defer.resolve(response);
                    console.info("Success! Response returned: " + JSON.stringify(response.data));
                  
                })
                .catch(function (err) {
                    defer.reject(err);
                    console.info("Error: " + JSON.stringify(err));
                   
                });
           return (defer.promise);
        }

        vm.intiBrand=function (brandInfo) {
            var defer = $q.defer();
            $http.get("/api/brand & product")
                .then(function (response) {
                    console.info("-- insert.controller.js > initBrand then() \n" + JSON.stringify(response.data));
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                    console.log("--  insert.controller.js > initBrand > error: \n" + JSON.stringify(err));
                });
            return (defer.promise);
        }
    }
})();