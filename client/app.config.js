(function () {
    angular
        .module("GroceryApp")
        .config(GroceryAppConfig)
    GroceryAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function GroceryAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl :'./insert/insert.html',
                controller : 'InsertCtrl',
                controllerAs : 'ctrl'
            })
            .state('B',{
                url : '/B',
                templateUrl :'./search/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'ctrl'
            })
            .state('C',{
                url : '/C',
                templateUrl :'./show/show.html',
                controller : 'ShowCtrl',
                controllerAs : 'ctrl'
            })
            .state('C1',{
                url : '/C1/:productNo',
                templateUrl :'./show/show.html',
                controller : 'ShowCtrl',
                controllerAs : 'ctrl'
            })

        $urlRouterProvider.otherwise("/A");


    }
})();