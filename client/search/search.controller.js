(function () {
    angular
        .module("GroceryApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http", "$state", "SearchService"];

    function SearchCtrl($http, $state, SearchService) {
        var vm = this;

        vm.brandName = '';
        vm.result = null;
        vm.showBrand = false;

        // Exposed functions
        vm.search = search;
        vm.searchForBrand = searchForBrand;

        // Initial calls
        init();


        // Function definitions
        function init() {
            SearchService.initProductData()
                .then(function (response) {
                    vm.product = response;
                })
                .catch(function (error) {
                    cosole.log("error " + error);
                })
        }

        function search() {
            vm.showBrand = false;
            SearchService.search( {
                params: {
                    'brand & product_name': vm.brand.productName
                }
            }).then(function (response) {
                vm.brands = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        function searchForBrand() {
            vm.showBrand = true;


            SearchService.search( {
                params: {
                    'brand & product_name': vm.brand.productName
                }
            }).then(function (response) {
                vm.brand.product = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        vm.showBrands = function (prodcutNo) {
            var brands = productNo;
            $state.go("C1", {productNo: brand.productNo})
        }
    }

})();