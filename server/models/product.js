module.exports = function(conn, Sequelize) {
    var Brands=  conn.define('brands', {
        brand_no: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        input_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        brand_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        product_name: {
            type: Sequelize.STRING,
            allowNull: false
        },


        Expiry_date: {
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {
        tableName: 'products',
        timestamps: false
    });
    return Products;
};