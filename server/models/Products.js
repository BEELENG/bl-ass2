//Create a model for brand_product table
module.exports = function (conn, Sequelize) {
    var Brand = conn.define('brand.product', {
            product_no: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'product',
                    key: 'brand.product_no'
                }
            },
            brand_no: {
                type: Sequelize.CHAR(4),
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'brands',
                    key: 'brand.product_no'
                }
            },
            from_date: {
                type: Sequelize.DATE,
                allowNull: false
            },
            to_date: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }, {
            timestamps: false,
            tableName: "brand.product"
        }
    );

    return Brand;
};