//Create a model for departments table
module.exports = function (conn, Sequelize) {
    var Brand = conn.define("brands", {
        brand_no: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        brand_name: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });

    return Brand;
};