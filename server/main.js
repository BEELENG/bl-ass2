//Load dependencies
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory  __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

//TODO :Create a connection with mysql DB <HERE>
var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = 'FSf3123!';
var conn = new Sequelize('grocery_list', MYSQL_USERNAME, MYSQL_PASSWORD, {});

var Brand = require('./models/brand')(conn, Sequelize);
var Products = require('./models/Products.js')(conn, Sequelize);
var Product = require('./models/product')(conn, Sequelize);


// Associations
Products.hasMany(Product, {foreignKey: 'Brand.Product_no'});
Product.hasOne(Brand, {foreignKey: 'brand_no'});

// For exercise: transactions
app.get("/api/brand.products", function (req, res) {
    console.log("-- GET /api/brand.products");
    BrandP
        .findAll({
            // Get all active products
            where: {to_date: "9999-01-01T00:00:00.000Z"}
            , attributes: ['product_no']
        })
        .then(function (products) {
            console.log("products: " + JSON.stringify(products));
            Product
                .findAll({
                    $notin: product        // Exclude those that are currently products of other brands
                    // buggy need to fix
                    , limit: 12             // limit to 12 bigint
                    , attributes: ['brand.product_no', 'brand', 'product_name'] // capture only these fields
                })
                .then(function (products) {
                    console.log("-- GET /api/products Products then() \n " + JSON.stringify(products));
                    res.json(products);
                })
                .catch(function (err) {
                    console.log("-- GET /api/products Products catch() \n " + JSON.stringify(err));
                    console.log("err " + err);
                    res
                        .status(500)
                        .json({error: true})
                });
        })
        .catch(function (err) {
            console.log("-- GET /api/Brand.products catch() \n " + JSON.stringify(err));
            console.log("err " + err)
            res
                .status(500)
                .json({error: true})
        });
});


app.get("/api/brand & products/:brand.product_no", function (req, res) {
    console.log("-- GET /api/brands/:brand_no");
    console.log("-- GET /api/brands/:brand_no request params: " + JSON.stringify(req.params));

    var where = {};
    if (req.params.dept_no) {
        where.dept_no = req.params.dept_no
    }

    Brand
        .findOne({
            where: where
            , include: [{
                model: Brand
                , order: [["to_date", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Product]
            }]
        })

        .then(function (brands) {
            console.log("-- GET /api/brand.products/:brand.product_no findOne then() result \n " + JSON.stringify(brands));
            res.json(brands);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("-- GET /api/departments/:dept_no findOne catch() \n " + JSON.stringify(brands));
            res
                .status(500)
                .json({error: true});
        });

});
// -- Updates product info
app.put('/api/brand & products/:brand.product_no', function (req, res) {
    console.log("-- PUT /api/brand & products/:brand.product_no");
    console.log("-- PUT /api/brand & products/:brand_no request params: " + JSON.stringify(req.params));
    console.log("-- PUT /api/brand & products/:brand_no request body: " + JSON.stringify(req.body));

    var where = {};
    where.brand_no = req.params.brand_no;

    // Updates brand detail
    Brand
        .update(
            {Brand_name: req.body.brand_name}             // assign new values
            , {where: where}                            // search condition / criteria
        )
        .then(function (response) {
            console.log("-- PUT /api/brand.products/:brand.product_no brand.update then(): \n"
                + JSON.stringify(response));
        })
        .catch(function (err) {
            console.log("-- PUT /api/brand.products/:brand.product_no brand.update catch(): \n"
                + JSON.stringify(err));
        });

});

app.delete("/api/brand.products/:brand.product_no/:brand.product_no", function (req, res) {


    console.log("-- DELETE /api/products/:product_no/:product_no");
    console.log("-- DELETE /api/products/:product_no/:product_no request params: " + JSON.stringify(req.params));
    var where = {};
    where.brand_no = req.params.brand_no;
    where.product_no = req.params.product_no;
    console.log("where " + JSON.stringify(where));


    Product
        .destroy({
            where: where
        })
        .then(function (result) {
            console.log("-- DELETE /api/products/:product_no/:product_no then(): \n" + JSON.stringify(result));
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});

        })
        .catch(function (err) {
            console.log("-- DELETE /api/products/:brand_no/:product_no catch(): \n" + JSON.stringify(err));
        });
});


app.post("/api/brands", function (req, res) {
    console.log("Query", req.query);
    console.log("Body", req.body);


    conn.transaction(function (t) {
        return product
            .create(
                {
                    brand_no: req.body.brand_no
                    , brand_name: req.body.brand_name
                }
                , {transaction: t})
            .then(function (result) {
                console.log("inner result " + JSON.stringify(result))
                return brand.product
                    .create(
                        {
                            brand_no: req.body.brand_no
                            , product_no: req.body.product
                            , from_date: req.body.from_date
                            , to_date: req.body.to_date
                        }
                        , {transaction: t});
            });
    })
        .then(function (result) {
            console.log("outer result " + JSON.stringify(result));
            res
                .status(200)
                .json(result);
        })
        .catch(function (err) {
            console.log("outer error: " + JSON.stringify(err));
            res
                .status(500)
                .json({error: true});
        });

    app.get("/api/brands", function (req, res) {
        var where = {};

        if (req.query.brand_name) {
            where.brand_name = {
                $like: "%" + req.query.brand_name + "%"
            }
        }

        if (req.query.brand_no) {
            where.brand_no = req.query.brand_no;
        }

        console.log("where: " + JSON.stringify(where));
        Brand
            .findAll({
                where: where
            })
            .then(function (Brands) {
                res.json(brands);
            })
            .catch(function () {
                res
                    .status(500)
                    .json({error: true})
            });

    });
    app.get("/api/brand.products", function (req, res) {
        var where = {};

        console.log("query: " + JSON.stringify(req.query));
        if (req.query.brand_name) {
            where.brand_name = {
                $like: "%" + req.query.brand_name + "%"
            }
        }
        Brand
            .findAll({
                where: where
                , include: [{
                    model: Brand
                    , order: [["to_date", "DESC"]]
                    , limit: 1
                    , include: [Product]
                }]
            })

            .then(function (brands) {
                console.log("brands " + JSON.stringify(brands));
                res.json(brands);
            })

            .catch(function (err) {
                console.log("error " + err);
                res
                    .status(500)
                    .json({error: true});
            })


        app.listen(3000, function () {
            console.info("Webserver started on port 3000");
        });
    });{}}